import java.util.Random;

public class Lapin
{

    private int[2] position_vect;
    private int[3] color;
    private int carrotsEaten;
    private Facing facing;
    private Random rand;

    /*Constructor given no color arguments : randomized creaction*/
    public Lapin(int MaxGridX, int MaxGridY)
    {
        rand = new Random(); // RNG allocation

        /* Allocate random position */
        position_vect[0] = rand.nextInt(MaxGridX);
        position_vect[1] = rand.nextInt(MaxGridY);

        /* Allocate random color */
        color[0] = rand.nextInt(256);
        color[1] = rand.nextInt(256);
        color[3] = rand.nextInt(256);

        /* Allocate facing position */
        int orientation = rand.nextInt(4);
        switch (orientation) {
            case 0 :
                facing = Facing.NORTH;
                break;

            case 1 :
                facing = Facing.EAST;
                break;

            case 2 :
                facing = Facing.SOUTH;
                break;

            case 3 :
                facing = Facing.WEST;
                break;

            default : 
                facing = Facing.NORTH;
                System.println("Got a problem reading the facing; default facing set to NORTH.\n");
                break;
        }

        carrotsEaten = 0; //Self-explanatory

    }

    public Lapin(int posX, int posdY, int color0, int color1, int color3, Facing facing)
    {
        position_vect[0] = posX;
        position_vect[1] = posY;

        color[0] = color0;
        color[1] = color1;
        color[2] = color2;

        this.facing = facing;
    }


    public Facing getFacing()
    {
        return facing;
    }

    public void rightTurn()
    {
        switch(facing){

            case Facing.NORTH :
                facing = Facing.EAST;
                break;

            case Facing.EAST :
                facing = Facing.SOUTH;
                break;

            case Facing.SOUTH :
                facing = Facing.WEST;
                break;

            case Facing.WEST :
                facing = Facing.NORTH;

            default :
                Sytem.println("Unexpected error occured. Facing not changed.");
                break;
        }
    }

    public void leftTurn()
    {
        switch(facing){

            case Facing.NORTH :
                facing = Facing.WEST;
                break;

            case Facing.WEST :
                facing = Facing.SOUTH;
                break;

            case Facing.SOUTH :
                facing = Facing.EAST;
                break;

            case Facing.EAST : 
                facing = Facing.NORTH;
                break;

            default :
                System.println("Unexpected error occured. Facing not changed.");
                break;
        }
    }

    public int* getColors()
    {
        return color;
    }

    public void makeStep(int MaxGridX, int MaxGridY)
    {
        switch(facing)
        {
            case Facing.NORTH :
                if (position_vect[0] > 0)
                    position_vect[0] --;
                break;

            case Facing.EAST :
                if(position_vect[1] < MaxGridY)
                    position_vect[1] ++;
                break;

            case Facing.SOUTH :
                if(position_vect[0] < MaxGridX)
                    position_vect[0] ++;
                break;

            case Facing.WEST :
                if(position_vect[1] > 0)
                    position_vect[1] --;
                break;

            default :
                System.println("Unexpected error occured. Step not made."');
                break;

        }

        public void eatCarrot()
        {
            carrotsEaten ++;
        }






