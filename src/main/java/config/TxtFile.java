package config;

/**
 * Configuration filename of txtfiles
 * 
 * @author Yann Ramusat
 * 
 */
public class TxtFile {
	public static final String TXT_JARDIN = "src/main/resources/Jardin.txt";
	public static final String TXT_LAPIN = "src/main/resources/Lapin.txt";
}
