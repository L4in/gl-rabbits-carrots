import element.mobile.Facing;

/**
 * The smallest element of the grid constituting the garden
 * 
 * @author Yann Ramusat
 * 
 */
public class Case {
	private Boolean rocher = false;
	private Boolean occuped = false;
	private Boolean occupedByRenard = false;
	private int numberOfCarottes = 0;
	private Facing facingOn;

	public Case() {
		// Empty. All the variables are the same each time you create a box
	}

	/**
	 * Check if there is a rock on the box
	 * 
	 * @return True if there is a rock of false if empty
	 */
	public Boolean getRocher() {
		return rocher;
	}

	/**
	 * Allow to place or remove a boulder on the box
	 * 
	 * @param rocher
	 *            True if we want a rock or false if we want to remove it
	 */
	public void setRocher(Boolean rocher) {
		this.rocher = rocher;
	}

	/**
	 * Get the number of carrots on the box
	 * 
	 * @return The number of carrots on the box
	 */
	public int getNumberOfCarottes() {
		return numberOfCarottes;
	}

	/**
	 * Set the number of remaining carrots on the box
	 * 
	 * @param numberOfCarottes
	 *            The number of carrots we want to place on the box
	 */
	public void setNumberOfCarottes(int numberOfCarottes) {
		this.numberOfCarottes = numberOfCarottes;
	}

	/**
	 * This fonction's call allow to remove one eaten carrot
	 */
	public void carotteMangee() {
		this.numberOfCarottes--;
	}

	/**
	 * Get if the box is occupied or not
	 * 
	 * @return True if the box is occupied, or false if empty
	 */
	public Boolean getOccuped() {
		return occuped;
	}

	/**
	 * Allow to set if the box is occuped of not
	 * 
	 * @param occuped
	 *            True if we want the box to be occupied, false if we want to
	 *            free it
	 */
	public void setOccuped(Boolean occuped) {
		this.occuped = occuped;
	}

	/**
	 * Function allowing to get the facing of an object on the box The facing is
	 * extrinsic to the object present on the case because it was much more
	 * simpler to get the facing from the box than building a complex system to
	 * get the reference of the element placed on the box
	 * 
	 * @return The facing of the element in the case
	 * @see element.mobile.Facing
	 */
	public Facing getFacingOn() {
		return facingOn;
	}

	/**
	 * Set the facing of the object on the box
	 * 
	 * @param facingOn
	 *            The facing we want to give
	 * @see element.mobile.Facing
	 */
	public void setFacingOn(Facing facingOn) {
		this.facingOn = facingOn;
	}

	/**
	 * Get if the box is occupied or not by a fox
	 * 
	 * @return True if the box is occupied by a fox, or false if empty
	 */
	public Boolean getOccupedByRenard() {
		return occupedByRenard;
	}

	/**
	 * Allow to set if the box is occuped of not by a fox
	 * 
	 * @param occuped
	 *            True if we want the box to be occuped by a fox, false if we
	 *            want to free it
	 */
	public void setOccupedByRenard(Boolean occupedByRenard) {
		this.occupedByRenard = occupedByRenard;
	}
}
