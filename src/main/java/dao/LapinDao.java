package dao;

import java.util.List;

import element.mobile.lapin.Lapin;

/**
 * DAO pour la gestion des lapins.
 * 
 * @author Yann Ramusat
 * 
 */
public interface LapinDao {
	public List<Lapin> getAllLapins();
}
