package dao.txt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import dao.JardinDao;
import element.fixe.Carotte;
import element.fixe.Rocher;

/**
 * Class for centralized data loading regarding garden status
 * 
 * @authors Alexandra Hu, Yann Ramusat
 * 
 */
public class TxtJardinDao implements JardinDao {

	// ################################################
	// ################# Attributs ####################
	// ################################################

	protected File file;
	protected int largeur;
	protected int hauteur;

	protected List<String> lignesCarottes = new ArrayList<String>();
	protected List<String> lignesRochers = new ArrayList<String>();

	protected List<Carotte> carottes;
	protected List<Rocher> rochers;

	private final static String SEPARATOR = " ";

	public void init(File file) {
		this.file = file;
		reloadJardin();
	}

	// ################################################
	// ############## Methodes diverses ###############
	// ################################################

	public void getLignesFromFile() {
		FileReader fr = null;
		BufferedReader br = null;
		try {
			fr = new FileReader(file);
			br = new BufferedReader(fr);

			for (String ligne = br.readLine(); ligne != null; ligne = br
					.readLine()) {

				// Suppression des espaces en trop
				ligne = ligne.trim();

				// Filtre des lignes vides
				if (ligne.isEmpty()) {
					continue;
				}

				// Filtre des lignes de commentaire
				if (ligne.startsWith("#")) {
					continue;
				}

				// Filtre des lignes de commentaire
				if (ligne.startsWith("J")) {
					final String[] values = ligne.split(SEPARATOR);
					largeur = Integer.parseInt(values[1]);
					hauteur = Integer.parseInt(values[2]);
				}

				// Filtre des lignes de commentaire
				if (ligne.startsWith("C")) {
					lignesCarottes.add(ligne);
				}

				// Filtre des lignes de commentaire
				if (ligne.startsWith("R")) {
					lignesRochers.add(ligne);
				}
			}
		} catch (IOException e) {

		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {

				}
			}
			if (fr != null) {
				try {
					fr.close();
				} catch (IOException e) {

				}
			}
		}
	}

	/**
	 * Read a string and create a carrot at the given coordinates
	 * 
	 * @param list
	 *            The list of strings containing the data
	 * @return The carrot created
	 * @throws Exception
	 *             NumberFormatException is thrown if the string parsing does
	 *             not return a proper number
	 * @see #transformLigneToCarotte(String)
	 */
	protected Carotte transformLigneToCarotte(final String[] list)
			throws Exception {

		final Carotte carotte = new Carotte();

		final String[] values = list[1].split("-");
		carotte.setLargeur(Integer.parseInt(values[0]));
		carotte.setHauteur(Integer.parseInt(values[1]));

		carotte.setNumber(Integer.parseInt(list[2]));

		return carotte;
	}

	/**
	 * Wrapping of {@link #transformLigneToCarotte(String[])} giving the correct
	 * arguments to this function
	 * 
	 * @param ligne
	 *            The line containing the information
	 * @return The carrot created
	 * @throws Exception
	 *             NumberFormatException is thrown if the string parsing does
	 *             not return a proper number
	 * @see #transformLigneToCarotte(String[])
	 */
	private Carotte transformLigneToCarotte(final String ligne)
			throws Exception {
		final String[] values = ligne.split(SEPARATOR);
		return transformLigneToCarotte(values);
	}

	/**
	 * Read a string and create a rock at the given coordinates
	 * 
	 * @param list
	 *            The list of strings containing the data
	 * @return The rock created
	 * @throws Exception
	 *             NumberFormatException is thrown if the string parsing does
	 *             not return a proper number
	 * @see #transformLigneToRocher(String[])
	 */
	protected Rocher transformLigneToRocher(final String[] list)
			throws Exception {

		final Rocher rocher = new Rocher();

		final String[] values = list[1].split("-");
		rocher.setLargeur(Integer.parseInt(values[0]));
		rocher.setHauteur(Integer.parseInt(values[1]));

		return rocher;
	}

	/**
	 * Wrapping of {@link #transformLigneToRocher(String[])}
	 * 
	 * @param ligne
	 *            The line containing the information
	 * @return The rock created
	 * @throws Exception
	 *             NumberFormatException is thrown if the string parsing does
	 *             not return a proper number
	 * @see #transformLigneToRocher(String[])
	 */
	private Rocher transformLigneToRocher(final String ligne) throws Exception {
		final String[] values = ligne.split(SEPARATOR);
		return transformLigneToRocher(values);
	}

	public void reloadJardin() {

		if (file == null) {
			throw new IllegalStateException("Le fichier est nul...");
		}

		try {
			getLignesFromFile();

			carottes = new ArrayList<Carotte>(lignesCarottes.size());
			for (String ligne : lignesCarottes) {
				final Carotte carotte = transformLigneToCarotte(ligne);
				carottes.add(carotte);
			}

			rochers = new ArrayList<Rocher>(lignesRochers.size());
			for (String ligne : lignesRochers) {
				final Rocher rocher = transformLigneToRocher(ligne);
				rochers.add(rocher);
			}

		} catch (Exception e) {

		}

	}

	// ################################################
	// ################ Getters #######################
	// ################################################

	/**
	 * Get the file containing the data
	 * 
	 * @return The file containing the data
	 */
	public File getFile() {
		return file;
	}

	@Override
	public int getLargeur() {
		return largeur;
	}

	@Override
	public int getHauteur() {
		return hauteur;
	}

	@Override
	public List<Carotte> getAllCarottes() {
		return carottes;
	}

	@Override
	public List<Rocher> getAllRochers() {
		return rochers;
	}
}