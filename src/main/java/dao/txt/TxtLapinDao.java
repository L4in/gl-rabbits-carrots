package dao.txt;

import java.io.File;
import java.util.List;

import element.mobile.Facing;
import element.mobile.lapin.Lapin;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Class for centralized data loading regarding rabbit statuses
 * 
 * @authors Alexandra Hu, Yann Ramusat
 * 
 */
public class TxtLapinDao {

	// ################################################
	// ################# Attributs ####################
	// ################################################

	protected File file;
	protected List<Lapin> lapins;

	private final static String SEPARATOR = " ";

	public void init(File file) {
		this.file = file;

		reloadLapins();
	}

	// ################################################
	// ############## Methodes diverses ###############
	// ################################################

	/**
	 * Transforms a file into a list of string
	 * 
	 * @return A list of string
	 */
	public List<String> getLignesFromFile() {

		final List<String> lignes = new ArrayList<String>();

		FileReader fr = null;
		BufferedReader br = null;
		try {
			fr = new FileReader(file);
			br = new BufferedReader(fr);

			for (String ligne = br.readLine(); ligne != null; ligne = br
					.readLine()) {

				// Suppression des espaces en trop
				ligne = ligne.trim();

				// Filtre des lignes vides
				if (ligne.isEmpty()) {
					continue;
				}

				// Filtre des lignes
				if (!ligne.startsWith("L")) {
					continue;
				}

				lignes.add(ligne);
			}
		} catch (IOException e) {

		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {

				}
			}
			if (fr != null) {
				try {
					fr.close();
				} catch (IOException e) {

				}
			}
		}
		return lignes;
	}

	/**
	 * Transform a list of string into a rabbit with position and movesets
	 * 
	 * @param list
	 *            An array of string containing the data
	 * @return A rock
	 * @throws Exception
	 *             NumberFormatException thrown in case of an error in parsing
	 * @see #transformLigneToLapin(String)
	 */
	protected Lapin transformLigneToLapin(final String[] list) throws Exception {

		final Lapin lapin = new Lapin();

		// POSITION X
		final String[] values = list[1].split("-");
		lapin.setLargeur(Integer.parseInt(values[0]));

		// POSITION Y
		lapin.setHauteur(Integer.parseInt(values[1]));

		// FACING
		switch (list[2]) {
		case "N":
			lapin.setFacing(Facing.NORTH);
			break;
		case "W":
			lapin.setFacing(Facing.WEST);
			break;
		case "S":
			lapin.setFacing(Facing.SOUTH);
			break;
		case "E":
			lapin.setFacing(Facing.EAST);
			break;
		default:
			lapin.setFacing(Facing.NORTH);
			break;
		}

		// LISTE DE MVMT
		for (int i = 0; i < list[3].length(); i++) {
			lapin.addMouvement(list[3].charAt(i));
		}

		lapin.setName(list[4]);

		return lapin;
	}

	/**
	 * Wrapping of {@link #transformLigneToLapin(String[])} ensuring parameters
	 * are correctly given
	 * 
	 * @param ligne
	 *            A line that need to be read
	 * @return A rock
	 * @throws Exception
	 *             NumberFormatException thrown in case of an error during the
	 *             parsing
	 */
	private Lapin transformLigneToLapin(final String ligne) throws Exception {
		final String[] values = ligne.split(SEPARATOR);
		return transformLigneToLapin(values);
	}

	/**
	 * Read rabbit data from file and refresh {@link #lapins}, the list of
	 * rabbits
	 */
	public void reloadLapins() {

		if (file == null) {
			throw new IllegalStateException("Le fichier est nul...");
		}

		try {
			final List<String> lignes = getLignesFromFile();

			lapins = new ArrayList<Lapin>(lignes.size());
			for (String ligne : lignes) {
				final Lapin lapin = transformLigneToLapin(ligne);
				lapins.add(lapin);
			}

		} catch (Exception e) {

		}

	}

	// ################################################
	// ################ Getters #######################
	// ################################################

	/**
	 * Get the file
	 * 
	 * @return The file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * Get the list of rabbits
	 * 
	 * @return The list of rabbits
	 */
	public List<Lapin> getAllLapins() {

		if (lapins == null) {
			throw new IllegalStateException(
					"La liste n'a pas encore ete initialisée...");
		}
		return lapins;
	}
}
