import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import element.mobile.Facing;
import element.mobile.lapin.Lapin;
import element.mobile.lapin.ModeleLapin;
import element.mobile.renard.Renard;

/**
 * Class integrating the main of the game and the functional loop for the game
 * @author Yann Ramusat
 *
 */
public class Game extends JFrame implements ActionListener {
	public Jardin jardin;
	public ModeleLapin lapins = new ModeleLapin();
	public List<Renard> renards = new ArrayList<Renard>();
	public JButton genetic;
	public JButton stats;
	public JFrame frame;
	public JPanel topPanel;
	public JPanel lowPanel;
	
	public String gameState = "basic"; // basic wait genetic
	
	private final JTable tableau;
	
	public Random rand = new Random();	
	
	public Game()
	{
		// Init du JPanel jardin
		jardin = new Jardin();
		
		// Affichage des lapins pour la premi�re occurence de run(), g�re les v�rifications des donn�es des lapins
		for(int i = 0; i < lapins.getLapins().size(); i++)
		{
			if(lapins.getLapins().get(i).getHauteur() >= jardin.cases.length || lapins.getLapins().get(i).getLargeur() >= jardin.cases[0].length || lapins.getLapins().get(i).getHauteur() < 1 || lapins.getLapins().get(i).getLargeur() < 1) 
			{
				//lapins.getLapins().remove(lapins.getLapins().get(i));
				lapins.getLapins().get(i).setDead(true);
			}
			else
			{
				if (!jardin.cases[lapins.getLapins().get(i).getHauteur()-1][lapins.getLapins().get(i).getLargeur()-1].getOccuped())
				{
					jardin.cases[lapins.getLapins().get(i).getHauteur()-1][lapins.getLapins().get(i).getLargeur()-1].setOccuped(true);
					jardin.cases[lapins.getLapins().get(i).getHauteur()-1][lapins.getLapins().get(i).getLargeur()-1].setFacingOn(lapins.getLapins().get(i).getFacing());
				}
				else
				{
					//lapins.getLapins().remove(lapins.getLapins().get(i));
					lapins.getLapins().get(i).setDead(true);
				}
			}
		}
		
		// RENDU GRAPHIQUE
		JFrame frame = new JFrame("Genetic Rabbits");
		//frame.setPreferredSize(new Dimension(640,480));
		frame.setPreferredSize(new Dimension((int)getToolkit().getScreenSize().getWidth(), ((int)getToolkit().getScreenSize().getHeight() - 40)));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		   
	    // CENTRER AFFICHAGE
		frame.add(this.jardin, BorderLayout.CENTER);

		topPanel = new JPanel();
	    topPanel.setLayout(new GridLayout(2,1));
	    
		    tableau = new JTable(lapins);
		    topPanel.add(tableau.getTableHeader(), BorderLayout.NORTH);
		    
		    	JScrollPane jScrollPane = new JScrollPane(tableau);
		    	jScrollPane.setPreferredSize(new Dimension(150, 150));
		        jScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		    
		    topPanel.add(jScrollPane, BorderLayout.NORTH);

		    JScrollPane jScrollPane2 = new JScrollPane(topPanel);
	        jScrollPane2.setPreferredSize(new Dimension(200, 200));
	        jScrollPane2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
	    
	    frame.add(jScrollPane, BorderLayout.NORTH);
	    
	    tableau.setAutoCreateRowSorter(true);
        final TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(tableau.getModel()); 
        tableau.setRowSorter(sorter);
        sorter.setSortable(0, false); // colonne "Nom" pas triable
		
		lowPanel = new JPanel();
	    lowPanel.setLayout(new GridLayout(2,1));
	    
			stats = new JButton("Analyser les statistiques");
			stats.setActionCommand("stats");
		    stats.addActionListener(this);
		    stats.setVisible(false);
			lowPanel.add(stats, BorderLayout.NORTH);
			
			genetic = new JButton("Commencer le mode Genetic");
			genetic.setActionCommand("genetic");
		    genetic.addActionListener(this);
			genetic.setEnabled(false);
			lowPanel.add(genetic, BorderLayout.SOUTH);

			frame.add(lowPanel, BorderLayout.SOUTH);
		    
        frame.pack();
		frame.setVisible(true);
	}
	
	/**
	 * 
	 */
	public void play()
	{
		try {
			Thread.sleep(500);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		while(true)
		{
			if(gameState == "basic")
			{
				Boolean end = true;
				for(Lapin lapin : lapins.getLapins())
				{
					if (!lapin.isDead())
					{
						if(lapin.lookMouvement() == 'X') lapin.setPaused(true);
						if(!lapin.isPaused()) end = false;
						if(!lapin.isEat() && jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].getNumberOfCarottes() > 0)
						{
							lapin.mangeCarotte();
							jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].carotteMangee();
						}
						else if (lapin.lookMouvement() == 'G')
						{
							lapin.getMouvement();
							lapin.leftTurn();
							jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setFacingOn(lapin.getFacing());
						}
						else if (lapin.lookMouvement() == 'D')
						{
							lapin.getMouvement();
							lapin.rightTurn();
							jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setFacingOn(lapin.getFacing());
						}
						else if (lapin.lookMouvement() == 'A')
						{
							// GERER L'AVANCEMENT
							if(lapin.getFacing() == Facing.NORTH)
							{
								if(lapin.getHauteur() > 1)
								{
									if(!jardin.cases[lapin.getHauteur()-2][lapin.getLargeur()-1].getRocher())
									{
										if(!jardin.cases[lapin.getHauteur()-2][lapin.getLargeur()-1].getOccuped())
										{
											// D�placement du lapin
											jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setOccuped(false);
											lapin.setHauteur(lapin.getHauteur()-1);
											jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setOccuped(true);
											
											// G�rer les nouveaux contenus des �tats du lapin et des cases
											lapin.getMouvement();
											lapin.setEat(false);
											jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setFacingOn(lapin.getFacing());
										}
										else
											lapin.setPaused(true);
									}
									else
										lapin.getMouvement();
								}
								else
									lapin.getMouvement();
							}
							else if (lapin.getFacing() == Facing.WEST)
							{
								if(lapin.getLargeur() > 1)
								{
									if(!jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-2].getRocher())
									{
										if(!jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-2].getOccuped())
										{
											// D�placement du lapin
											jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setOccuped(false);
											lapin.setLargeur(lapin.getLargeur()-1);
											jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setOccuped(true);
											
											// G�rer les nouveaux contenus des �tats du lapin et des cases
											lapin.getMouvement();
											lapin.setEat(false);
											jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setFacingOn(lapin.getFacing());
										}
										else
											lapin.setPaused(true);
									}
									else
										lapin.getMouvement();
								}
								else
									lapin.getMouvement();
							}
							else if (lapin.getFacing() == Facing.SOUTH)
							{
								if(lapin.getHauteur() < jardin.getHauteur())
								{
									if(!jardin.cases[lapin.getHauteur()][lapin.getLargeur()-1].getRocher())
									{
										if(!jardin.cases[lapin.getHauteur()][lapin.getLargeur()-1].getOccuped())
										{
											// D�placement du lapin
											jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setOccuped(false);
											lapin.setHauteur(lapin.getHauteur()+1);
											jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setOccuped(true);
											
											// G�rer les nouveaux contenus des �tats du lapin et des cases
											lapin.getMouvement();
											lapin.setEat(false);
											jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setFacingOn(lapin.getFacing());
										}
										else
											lapin.setPaused(true);
									}
									else
										lapin.getMouvement();
								}
								else
									lapin.getMouvement();	
							}
							else if (lapin.getFacing() == Facing.EAST)
							{
								if(lapin.getLargeur() < jardin.getLargeur())
								{
									if(!jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()].getRocher())
									{
										if(!jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()].getOccuped())
										{
											// D�placement du lapin
											jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setOccuped(false);
											lapin.setLargeur(lapin.getLargeur()+1);
											jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setOccuped(true);
											
											// G�rer les nouveaux contenus des �tats du lapin et des cases
											lapin.getMouvement();
											lapin.setEat(false);
											jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setFacingOn(lapin.getFacing());
										}
										else
											lapin.setPaused(true);
									}
									else
										lapin.getMouvement();
								}
								else
									lapin.getMouvement();
							}
						}
					}
					
					if(end) 
					{
						gameState = "wait";
						genetic.setEnabled(true);
					}
				}
				
				try
				{
					Thread.sleep(1000);
				} catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
			
			if(gameState == "wait")
			{
				
			}
			
			if(gameState == "genetic")
			{
				try {
					Thread.sleep(500);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				//for(Lapin lapin : lapins.getLapins())
				for(int i = 0; i < lapins.getLapins().size(); i++)
				{
					if(!lapins.getLapins().get(i).isDead())
					{
						int f = rand.nextInt(4);
						switch(f)
						{
							case 0:
								lapins.getLapins().get(i).leftTurn();
								jardin.cases[lapins.getLapins().get(i).getHauteur()-1][lapins.getLapins().get(i).getLargeur()-1].setFacingOn(lapins.getLapins().get(i).getFacing());
								break;
							case 1:
								lapins.getLapins().get(i).rightTurn();
								jardin.cases[lapins.getLapins().get(i).getHauteur()-1][lapins.getLapins().get(i).getLargeur()-1].setFacingOn(lapins.getLapins().get(i).getFacing());
								break;
							default:
								if(lapins.getLapins().get(i).getFacing() == Facing.NORTH)
								{
									if(lapins.getLapins().get(i).getHauteur() > 1)
										if(!jardin.cases[lapins.getLapins().get(i).getHauteur()-2][lapins.getLapins().get(i).getLargeur()-1].getRocher())
											if(!jardin.cases[lapins.getLapins().get(i).getHauteur()-2][lapins.getLapins().get(i).getLargeur()-1].getOccuped())
												if(!jardin.cases[lapins.getLapins().get(i).getHauteur()-2][lapins.getLapins().get(i).getLargeur()-1].getOccupedByRenard())
												{
													// D�placement du lapin
													jardin.cases[lapins.getLapins().get(i).getHauteur()-1][lapins.getLapins().get(i).getLargeur()-1].setOccuped(false);
													lapins.getLapins().get(i).setHauteur(lapins.getLapins().get(i).getHauteur()-1);
													jardin.cases[lapins.getLapins().get(i).getHauteur()-1][lapins.getLapins().get(i).getLargeur()-1].setOccuped(true);
													
													// G�rer les nouveaux contenus des �tats du lapin et des cases
													jardin.cases[lapins.getLapins().get(i).getHauteur()-1][lapins.getLapins().get(i).getLargeur()-1].setFacingOn(lapins.getLapins().get(i).getFacing());
												}
											
								}
								else if (lapins.getLapins().get(i).getFacing() == Facing.WEST)
								{
									if(lapins.getLapins().get(i).getLargeur() > 1)
										if(!jardin.cases[lapins.getLapins().get(i).getHauteur()-1][lapins.getLapins().get(i).getLargeur()-2].getRocher())
											if(!jardin.cases[lapins.getLapins().get(i).getHauteur()-1][lapins.getLapins().get(i).getLargeur()-2].getOccuped())
												if(!jardin.cases[lapins.getLapins().get(i).getHauteur()-1][lapins.getLapins().get(i).getLargeur()-2].getOccupedByRenard())
												{
													// D�placement du lapin
													jardin.cases[lapins.getLapins().get(i).getHauteur()-1][lapins.getLapins().get(i).getLargeur()-1].setOccuped(false);
													lapins.getLapins().get(i).setLargeur(lapins.getLapins().get(i).getLargeur()-1);
													jardin.cases[lapins.getLapins().get(i).getHauteur()-1][lapins.getLapins().get(i).getLargeur()-1].setOccuped(true);
													
													// G�rer les nouveaux contenus des �tats du lapin et des cases
													jardin.cases[lapins.getLapins().get(i).getHauteur()-1][lapins.getLapins().get(i).getLargeur()-1].setFacingOn(lapins.getLapins().get(i).getFacing());
												}				
								}
								else if (lapins.getLapins().get(i).getFacing() == Facing.SOUTH)
								{
									if(lapins.getLapins().get(i).getHauteur() < jardin.getHauteur())
										if(!jardin.cases[lapins.getLapins().get(i).getHauteur()][lapins.getLapins().get(i).getLargeur()-1].getRocher())
											if(!jardin.cases[lapins.getLapins().get(i).getHauteur()][lapins.getLapins().get(i).getLargeur()-1].getOccuped())
												if(!jardin.cases[lapins.getLapins().get(i).getHauteur()][lapins.getLapins().get(i).getLargeur()-1].getOccupedByRenard())
												{
													// D�placement du lapin
													jardin.cases[lapins.getLapins().get(i).getHauteur()-1][lapins.getLapins().get(i).getLargeur()-1].setOccuped(false);
													lapins.getLapins().get(i).setHauteur(lapins.getLapins().get(i).getHauteur()+1);
													jardin.cases[lapins.getLapins().get(i).getHauteur()-1][lapins.getLapins().get(i).getLargeur()-1].setOccuped(true);
													
													// G�rer les nouveaux contenus des �tats du lapin et des cases
													jardin.cases[lapins.getLapins().get(i).getHauteur()-1][lapins.getLapins().get(i).getLargeur()-1].setFacingOn(lapins.getLapins().get(i).getFacing());
												}
								}
								else if (lapins.getLapins().get(i).getFacing() == Facing.EAST)
								{
									if(lapins.getLapins().get(i).getLargeur() < jardin.getLargeur())
										if(!jardin.cases[lapins.getLapins().get(i).getHauteur()-1][lapins.getLapins().get(i).getLargeur()].getRocher())
											if(!jardin.cases[lapins.getLapins().get(i).getHauteur()-1][lapins.getLapins().get(i).getLargeur()].getOccuped())
												if(!jardin.cases[lapins.getLapins().get(i).getHauteur()-1][lapins.getLapins().get(i).getLargeur()].getOccupedByRenard())
												{
													// D�placement du lapin
													jardin.cases[lapins.getLapins().get(i).getHauteur()-1][lapins.getLapins().get(i).getLargeur()-1].setOccuped(false);
													lapins.getLapins().get(i).setLargeur(lapins.getLapins().get(i).getLargeur()+1);
													jardin.cases[lapins.getLapins().get(i).getHauteur()-1][lapins.getLapins().get(i).getLargeur()-1].setOccuped(true);
													
													// G�rer les nouveaux contenus des �tats du lapin et des cases
													jardin.cases[lapins.getLapins().get(i).getHauteur()-1][lapins.getLapins().get(i).getLargeur()-1].setFacingOn(lapins.getLapins().get(i).getFacing());
												}
								}			
						}
						
						// Reproduction
						if(lapins.getLapins().get(i).getHauteur() > 1)
							if(!jardin.cases[lapins.getLapins().get(i).getHauteur()-2][lapins.getLapins().get(i).getLargeur()-1].getRocher())
								if(!jardin.cases[lapins.getLapins().get(i).getHauteur()-2][lapins.getLapins().get(i).getLargeur()-1].getOccuped())
									if(!jardin.cases[lapins.getLapins().get(i).getHauteur()-2][lapins.getLapins().get(i).getLargeur()-1].getOccupedByRenard())
										if(lapins.getLapins().get(i).hasReproduced() && lapins.getLapins().get(i).hasReproduced() && lapins.getLapins().get(i).hasReproduced())
										{
											lapins.getLapins().get(i).enfanter();
											lapins.getLapins().add(new Lapin(lapins.getLapins().get(i), lapins.getLapins().get(i).getHauteur()-1, lapins.getLapins().get(i).getLargeur()));
											jardin.cases[lapins.getLapins().get(i).getHauteur()-2][lapins.getLapins().get(i).getLargeur()-1].setOccuped(true);
										}
					}
				}

				for(Renard renard : renards)
				{
					int f = rand.nextInt(4);
					switch(f)
					{
						case 0:
							renard.leftTurn();
							jardin.cases[renard.getHauteur()-1][renard.getLargeur()-1].setFacingOn(renard.getFacing());
							break;
						case 1:
							renard.rightTurn();
							jardin.cases[renard.getHauteur()-1][renard.getLargeur()-1].setFacingOn(renard.getFacing());
							break;
						default:
							if(renard.getFacing() == Facing.NORTH)
							{
								if(renard.getHauteur() > 1)
									if(!jardin.cases[renard.getHauteur()-2][renard.getLargeur()-1].getRocher())
										if(!jardin.cases[renard.getHauteur()-2][renard.getLargeur()-1].getOccuped())
											if(!jardin.cases[renard.getHauteur()-2][renard.getLargeur()-1].getOccupedByRenard())
											{
												// D�placement du renard
												jardin.cases[renard.getHauteur()-1][renard.getLargeur()-1].setOccupedByRenard(false);
												renard.setHauteur(renard.getHauteur()-1);
												jardin.cases[renard.getHauteur()-1][renard.getLargeur()-1].setOccupedByRenard(true);
												
												// G�rer les nouveaux contenus des �tats du renard et des cases
												jardin.cases[renard.getHauteur()-1][renard.getLargeur()-1].setFacingOn(renard.getFacing());
											}
										
							}
							else if (renard.getFacing() == Facing.WEST)
							{
								if(renard.getLargeur() > 1)
									if(!jardin.cases[renard.getHauteur()-1][renard.getLargeur()-2].getRocher())
										if(!jardin.cases[renard.getHauteur()-1][renard.getLargeur()-2].getOccuped())
											if(!jardin.cases[renard.getHauteur()-1][renard.getLargeur()-2].getOccupedByRenard())
											{
												// D�placement du renard
												jardin.cases[renard.getHauteur()-1][renard.getLargeur()-1].setOccupedByRenard(false);
												renard.setLargeur(renard.getLargeur()-1);
												jardin.cases[renard.getHauteur()-1][renard.getLargeur()-1].setOccupedByRenard(true);
												
												// G�rer les nouveaux contenus des �tats du renard et des cases
												jardin.cases[renard.getHauteur()-1][renard.getLargeur()-1].setFacingOn(renard.getFacing());
											}				
							}
							else if (renard.getFacing() == Facing.SOUTH)
							{
								if(renard.getHauteur() < jardin.getHauteur())
									if(!jardin.cases[renard.getHauteur()][renard.getLargeur()-1].getRocher())
										if(!jardin.cases[renard.getHauteur()][renard.getLargeur()-1].getOccuped())
											if(!jardin.cases[renard.getHauteur()][renard.getLargeur()-1].getOccupedByRenard())
											{
												// D�placement du renard
												jardin.cases[renard.getHauteur()-1][renard.getLargeur()-1].setOccupedByRenard(false);
												renard.setHauteur(renard.getHauteur()+1);
												jardin.cases[renard.getHauteur()-1][renard.getLargeur()-1].setOccupedByRenard(true);
												
												// G�rer les nouveaux contenus des �tats du renard et des cases
												jardin.cases[renard.getHauteur()-1][renard.getLargeur()-1].setFacingOn(renard.getFacing());
											}
							}
							else if (renard.getFacing() == Facing.EAST)
							{
								if(renard.getLargeur() < jardin.getLargeur())
									if(!jardin.cases[renard.getHauteur()-1][renard.getLargeur()].getRocher())
										if(!jardin.cases[renard.getHauteur()-1][renard.getLargeur()].getOccuped())
											if(!jardin.cases[renard.getHauteur()-1][renard.getLargeur()].getOccupedByRenard())
											{
												// D�placement du renard
												jardin.cases[renard.getHauteur()-1][renard.getLargeur()-1].setOccupedByRenard(false);
												renard.setLargeur(renard.getLargeur()+1);
												jardin.cases[renard.getHauteur()-1][renard.getLargeur()-1].setOccupedByRenard(true);
												
												// G�rer les nouveaux contenus des �tats du renard et des cases
												jardin.cases[renard.getHauteur()-1][renard.getLargeur()-1].setFacingOn(renard.getFacing());
											}
							}			
					}
					
					// Gestion des attaques
					if(renard.getHauteur() > 1 && jardin.cases[renard.getHauteur()-2][renard.getLargeur()-1].getOccuped())
					{
						for(Lapin lapin : lapins.getLapins())
						{
							if(lapin.getHauteur() == renard.getHauteur() - 1 && lapin.getLargeur() == renard.getLargeur() && !lapin.isDead())
							{
								if(!lapin.isAliveAfterFoxEncounter())
								{
									lapin.setDead(true);
									//jardin.cases[renard.getHauteur()-2][renard.getLargeur()-1].setOccuped(false);
									jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setOccuped(false);
								}
								else
								{
									lapin.makeEsquive();
								}
							}
						}
					}
					else if(renard.getLargeur() > 1 && jardin.cases[renard.getHauteur()-1][renard.getLargeur()-2].getOccuped())
					{
						for(Lapin lapin : lapins.getLapins())
						{
							if(lapin.getHauteur() == renard.getHauteur() && lapin.getLargeur() == renard.getLargeur() - 1 && !lapin.isDead())
							{
								if(!lapin.isAliveAfterFoxEncounter())
								{
									lapin.setDead(true);
									//jardin.cases[renard.getHauteur()-1][renard.getLargeur()-2].setOccuped(false);
									jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setOccuped(false);
								}
								else
								{
									lapin.makeEsquive();
								}
							}
						}
					}
					else if(renard.getHauteur() < jardin.getHauteur() && jardin.cases[renard.getHauteur()][renard.getLargeur()-1].getOccuped())
					{
						for(Lapin lapin : lapins.getLapins())
						{
							if(lapin.getHauteur() == renard.getHauteur() + 1 && lapin.getLargeur() == renard.getLargeur() && !lapin.isDead())
							{
								if(!lapin.isAliveAfterFoxEncounter())
								{
									lapin.setDead(true);
									//jardin.cases[renard.getHauteur()][renard.getLargeur()-1].setOccuped(false);
									jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setOccuped(false);
								}
								else
								{
									lapin.makeEsquive();
								}
							}
						}
					}
					else if(renard.getLargeur() < jardin.getLargeur() && jardin.cases[renard.getHauteur()-1][renard.getLargeur()].getOccuped())
					{
						for(Lapin lapin : lapins.getLapins())
						{
							if(lapin.getHauteur() == renard.getHauteur() && lapin.getLargeur() == renard.getLargeur() + 1 && !lapin.isDead())
							{		
								if(!lapin.isAliveAfterFoxEncounter())
								{
									lapin.setDead(true);
									//jardin.cases[renard.getHauteur()-1][renard.getLargeur()].setOccuped(false);
									jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setOccuped(false);
								}
								else
								{
									lapin.makeEsquive();
								}
							}
						}
					}	
				}	
			}
			
			// g�rer la fin du jeu
			boolean allDead = true;
			for(Lapin lapin : lapins.getLapins())
			{
				if (!lapin.isDead()) allDead = false;
			}
			if (allDead) gameState = "wait";
			
			// Mise a jour graphique
			jardin.updateUI();
			lapins.fireTableDataChanged();
		}
	}
	
	/**
	 * MAIN of the program doing nothing except launching play()
	 * @param args	The array of strings containing arguments given
	 */
	public static void main(String[] args)
	{
		new Game().play();
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		switch(e.getActionCommand()) 
		{ 
			case "genetic": 	
				initialiseGeneticStep();
				gameState = "genetic";
				break;
		}
	}
	
	/**
	 * 
	 */
	public void initialiseGeneticStep()
	{
		//topPanel.setVisible(false);
		stats.setVisible(true);
		genetic.setLabel("Ajouter un renard");
		
		// initialiser la liste des renards
		int nbRenards = 1;
		for(int i = 0; i < nbRenards; i++)
		{
			boolean good = false;
			int h = 0;
			int l = 0;
			int nbEssais = 0;
			while(!good && nbEssais < jardin.getHauteur()*jardin.getLargeur())
			{
				h = rand.nextInt(jardin.getHauteur());
				l = rand.nextInt(jardin.getLargeur());
				
				if(!jardin.cases[h][l].getRocher() && !jardin.cases[h][l].getOccuped() && !jardin.cases[h][l].getOccupedByRenard())
					good = true;
				
				nbEssais++;
			}
			
			int f = rand.nextInt(4);
			switch(f)
			{
				case 0:
					renards.add(new Renard(h+1, l+1, Facing.NORTH));
					jardin.cases[h][l].setFacingOn(Facing.NORTH);
					break;
				case 1:
					renards.add(new Renard(h+1, l+1, Facing.WEST));
					jardin.cases[h][l].setFacingOn(Facing.WEST);
					break;
				case 2:
					renards.add(new Renard(h+1, l+1, Facing.SOUTH));
					jardin.cases[h][l].setFacingOn(Facing.SOUTH);
					break;
				default:
					renards.add(new Renard(h+1, l+1, Facing.EAST));
					jardin.cases[h][l].setFacingOn(Facing.EAST);
					break;
			}
			jardin.cases[h][l].setOccupedByRenard(true);
		}
		
		for(int i = 1; i <= jardin.cases[0].length; i++)
		{
			for(int j = 1; j <= jardin.cases.length; j++)
			{
				if(jardin.cases[j-1][i-1].getNumberOfCarottes() > 0) jardin.cases[j-1][i-1].setNumberOfCarottes(0);
			}
		}
	}
}
