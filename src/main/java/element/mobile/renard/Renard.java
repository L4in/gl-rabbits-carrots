package element.mobile.renard;

import element.mobile.ElementMobile;
import element.mobile.Facing;

/**
 * Simple class for fox emulation
 * 
 * @author Yann Ramusat
 * 
 */
public class Renard extends ElementMobile {
	public Renard(int hauteur, int largeur, Facing facing) {
		this.hauteur = hauteur;
		this.largeur = largeur;
		this.facing = facing;
	}
}
