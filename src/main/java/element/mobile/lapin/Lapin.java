package element.mobile.lapin;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import element.mobile.ElementMobile;
import element.mobile.Facing;

/**
 * Extension of ElementMobile for rabbit handling
 * 
 * @authors Cl�ment Michel, Yann Ramusat
 * 
 */
public class Lapin extends ElementMobile {

	private String name;
	private List<Character> mouvements = new ArrayList<Character>();
	private boolean paused = false;
	private boolean eat = false;
	private int carottesMangees = 0;
	private int[] colors = new int[3];
	private int esquives = 0;

	private int generation_number;
	private boolean dead = false;
	private int nbEnfants = 0;

	private static int lapinsGeneres = 0;

	Random rand = new Random();

	public Lapin() {
		/* Allocate random color */
		colors[0] = rand.nextInt(256);
		colors[1] = rand.nextInt(256);
		colors[2] = rand.nextInt(256);

		generation_number = 0;
	}

	public Lapin(Lapin parent, int hauteur, int largeur) {
		/* Allocate color based on parent */
		colors[0] = parent.getRedComponent() + (rand.nextInt(50) - 25);
		colors[1] = parent.getGreenComponent() + (rand.nextInt(50) - 25);
		colors[2] = parent.getBlueComponent() + (rand.nextInt(50) - 25);

		// Capped colors, we verify that the color is in [0,255]

		if (colors[0] < 0)
			colors[0] = 0;

		if (colors[0] > 255)
			colors[0] = 255;

		if (colors[1] < 0)
			colors[1] = 0;

		if (colors[1] > 255)
			colors[1] = 255;

		if (colors[2] < 0)
			colors[2] = 0;

		if (colors[2] > 255)
			colors[2] = 255;

		generation_number = parent.getGenerationNumber() + 1;

		this.hauteur = hauteur;
		this.largeur = largeur;

		int f = rand.nextInt(4);
		switch (f) {
		case 0:
			facing = Facing.WEST;
			break;
		case 1:
			facing = Facing.SOUTH;
			break;
		case 2:
			facing = Facing.EAST;
			break;
		case 3:
			facing = Facing.NORTH;
			break;
		default:
			System.out.println("Unexpected error occured. Facing not changed.");
			break;
		}

		this.setName("Genetic Rabbit " + lapinsGeneres);
		lapinsGeneres++;
	}

	/**
	 * Give the rabbit name (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return name;
	}

	/**
	 * Give the rabbit name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set the rabbit's name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Add movement to list of movements
	 */
	public void addMouvement(char mouvement) {
		mouvements.add(Character.valueOf(mouvement));
	}

	/**
	 * Get the next movement in the list of movement and remove it from the list
	 * 
	 * @return 'X' in case of no more moves, the movement if not empty
	 */
	public char getMouvement() {
		if (mouvements.size() == 0)
			return 'X';
		setPaused(false);
		return mouvements.remove(0);
	}

	/**
	 * Get the next movement in the list of movement, but not remove it
	 * 
	 * @return 'X' in case of no more moves, the movement if not empty
	 */
	public char lookMouvement() {
		if (mouvements.size() == 0)
			return 'X';
		return mouvements.get(0);
	}

	/**
	 * Get the number of eaten carrots
	 * 
	 * @return The number of eaten carrots
	 */
	public int getCarottesMangees() {
		return carottesMangees;
	}

	/**
	 * Eat a carrot and update the carrot number
	 */
	public void mangeCarotte() {
		this.carottesMangees++;
		this.setEat(true);
	}

	/**
	 * Check if the rabbit is paused
	 * 
	 * @return true if paused, false if not
	 */
	public boolean isPaused() {
		return paused;
	}

	/**
	 * Set the value of paused to the given value
	 * 
	 * @param paused
	 *            True to pause, false to unpause
	 */
	public void setPaused(boolean paused) {
		this.paused = paused;
	}

	/**
	 * Check if the rabbit has eaten
	 * 
	 * @return The state of eat
	 */
	public boolean isEat() {
		return eat;
	}

	/**
	 * Set the value of eat
	 * 
	 * @param eat
	 *            The value to be set
	 */
	public void setEat(boolean eat) {
		this.eat = eat;
	}

	/**
	 * Get the red component of the color
	 * 
	 * @return RGB [0-255] integer
	 */
	public int getRedComponent() {
		return colors[0];
	}

	/**
	 * Get the green component of the color
	 * 
	 * @return RGB [0-255] integer
	 */
	public int getGreenComponent() {
		return colors[1];
	}

	/**
	 * Get the blue component of the color
	 * 
	 * @return RGB [0-255] integer
	 */
	public int getBlueComponent() {
		return colors[2];
	}

	/**
	 * Get the generation number of the rabbit
	 * 
	 * @return The generation number
	 */
	public int getGenerationNumber() {
		return generation_number;
	}

	/**
	 * If you read this function, you're trying to alterate the class. Anyway,
	 * return the state of the agression
	 * 
	 * @return If the rabbit was attacked, return true, or false in the other
	 *         case
	 */
	private boolean isAgressed() {
		if (getBlueComponent() + 128 < rand.nextInt(512))
			return true;
		return false;
	}

	/**
	 * Return if the rabbit is killed by an agression
	 * 
	 * @return True if the rabbit is killed, false if not
	 */
	private boolean isKilledByAgression() {
		if (getRedComponent() + 128 < rand.nextInt(512))
			return true;
		return false;
	}

	/**
	 * Return if the rabbit was able to reproduce
	 * 
	 * @return True if the rabbit reproduced, false if not
	 */
	public boolean hasReproduced() {
		if (getGreenComponent() + 128 > rand.nextInt(512))
			return true;
		return false;
	}

	/**
	 * All-in method to determine the outcome of an encounter with a fox
	 * 
	 * @return True if the rabbit is alive after, false if he's dead
	 */
	public boolean isAliveAfterFoxEncounter() {
		if (isAgressed())
			if (isKilledByAgression())
				return false;
		return true;
	}

	/**
	 * Check if the rabbit is dead
	 * 
	 * @return True if the rabbit is dead of false if alive
	 */
	public boolean isDead() {
		return dead;
	}

	/**
	 * Gdo-mode function allowing to kill or resurrect a rabbit
	 * 
	 * @param dead
	 *            The state of life (false) or death(true) that have to be given
	 *            to the rabbit
	 */
	public void setDead(boolean dead) {
		this.dead = dead;
	}

	/**
	 * Getter du esquive
	 * 
	 * @return esquives The number of esquive
	 */
	public int getEsquives() {
		return esquives;
	}

	/**
	 * Add one esquive to the rabbit
	 * 
	 */
	public void makeEsquive() {
		this.esquives++;
	}

	/**
	 * Getter du nbEnfants
	 * 
	 * @return nbEnfants Number of rabbit' child
	 */
	public int getNbEnfants() {
		return nbEnfants;
	}

	/**
	 * The rabbit reproduce
	 */
	public void enfanter() {
		this.nbEnfants++;
	}
}
