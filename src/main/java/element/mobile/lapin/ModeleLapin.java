package element.mobile.lapin;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import config.TxtFile;
import service.GeneralService;

/**
 * 
 * @author Yann Ramusat
 * 
 */
public class ModeleLapin extends AbstractTableModel {
	private String[] entetes;
	private List<Lapin> lapins;

	final String fileName = TxtFile.TXT_LAPIN;

	private GeneralService service = GeneralService.getInstance();

	public ModeleLapin() {
		super();

		lapins = service.getAllLapins(fileName);
		// entetes = new String[] { "Nom", "Nombre de carottes mang�es" };
		entetes = new String[] { "Nom", "Nombre de carottes mang�es",
				"G�n�ration", "Nombre d'enfants", "Nombre d'esquives",
				"Taux d'esquive", "Taux de furtivit�", "Taux de reproduction",
				"D�c�d�" };
	}

	@Override
	public String getColumnName(int columnIndex) {
		return entetes[columnIndex];
	}

	@Override
	public int getRowCount() {
		return lapins.size();
	}

	@Override
	public int getColumnCount() {
		return entetes.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		final Lapin lapin = lapins.get(rowIndex);

		// Ordre : "Nom", "Nombre de carottes mang�es"
		switch (columnIndex) {
		case 0:
			return lapin.getName();
		case 1:
			return lapin.getCarottesMangees();
		case 2:
			return lapin.getGenerationNumber();
		case 3:
			return lapin.getNbEnfants();
		case 4:
			return lapin.getEsquives();
		case 5:
			return lapin.getRedComponent();
		case 6:
			return lapin.getGreenComponent();
		case 7:
			return lapin.getBlueComponent();
		case 8:
			return lapin.isDead();
		default:
			throw new IllegalArgumentException(
					"Le numero de colonne indique n'est pas valide.");
		}
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
		case 0:
			return String.class;
		case 1:
			return Integer.class;
		case 2:
			return Integer.class;
		case 3:
			return Integer.class;
		case 4:
			return Integer.class;
		case 5:
			return Integer.class;
		case 6:
			return Integer.class;
		case 7:
			return Integer.class;
		case 8:
			return Boolean.class;
		default:
			return Object.class;
		}
	}

	/**
	 * Get the list of rabbits in the game
	 * 
	 * @return The list of rabbits
	 */
	public List<Lapin> getLapins() {
		return lapins;
	}
}
