package dao;

import static org.junit.Assert.*;

import java.io.File;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import dao.txt.TxtJardinDao;
import element.fixe.Carotte;

public class JardinDaoTest {
	protected TxtJardinDao dao = new TxtJardinDao();

	@Before
	public void doBefore() {
		final File file = new File("src/test/resources/TestJardin.txt");
		dao.init(file);
	}

	/**
	 * Teste le nombre de carottes trouvees.
	 * 
	 * RESULT Nombre carottes : 2
	 */
	@Test
	public void testDeuxCarottes() {
		// Arrange
		final int nombreCarottesAttendues = 2;

		// Act
		final List<Carotte> carottes = dao.getAllCarottes();

		// Assert
		Assert.assertEquals(nombreCarottesAttendues, carottes.size());
	}

	/**
	 * Teste la largeur du jardin.
	 * 
	 * RESULT Largeur : 6
	 */
	@Test
	public void testLargeur() {
		// Arrange
		final int largeur = 6;

		// Assert
		Assert.assertEquals(largeur, dao.getLargeur());
	}

	/**
	 * Teste la hauteur du jardin.
	 * 
	 * RESULT Hauteur : 5
	 */
	@Test
	public void testHauteur() {
		// Arrange
		final int hauteur = 5;

		// Assert
		Assert.assertEquals(hauteur, dao.getHauteur());
	}
}
