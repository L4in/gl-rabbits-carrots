package dao;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import dao.txt.TxtLapinDao;
import element.mobile.lapin.Lapin;

public class LapinDaoTest {
	protected TxtLapinDao dao = new TxtLapinDao();

	@Before
	public void doBefore() {
		final File file = new File("src/test/resources/TestLapin.txt");
		dao.init(file);
	}

	/**
	 * Teste le nombre de lapins trouves.
	 * 
	 * RESULT Nombre lapins : 2
	 */
	@Test
	public void testDeuxLapins() {
		// Arrange
		final int nombreLapinsAttendus = 2;

		// Act
		final List<Lapin> lapins = dao.getAllLapins();

		// Assert
		Assert.assertEquals(nombreLapinsAttendus, lapins.size());
	}
}
