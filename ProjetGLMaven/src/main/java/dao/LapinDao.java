package dao;

import java.util.List;

import element.mobile.lapin.Lapin;

/**
 * DAO pour la gestion des lapins.
 * 
 * @authors
 * 
 */
public interface LapinDao {	
    public List<Lapin> getAllLapins();
}
