package dao;

import java.util.List;

import element.fixe.Carotte;
import element.fixe.Rocher;

public interface JardinDao {
	public int getLargeur();
	public int getHauteur();
	
	public List<Carotte> getAllCarottes();
    public List<Rocher> getAllRochers();
}
