package element.mobile;

import element.Element;

/**
 * Extension of Element allowing to handle mobile elements
 * @author Yann Ramusat
 * @see element.Element
 */
public abstract class ElementMobile extends Element {
	protected Facing facing;

	/**
	 * Get the current facing of the element
	 * @return	The facing of the element
	 * @see Facing
	 */
	public Facing getFacing()
	{
		return this.facing;
	}

	/**
	 * Set the facing of the element
	 * @param facing	The facing we want to give
	 * @see Facing
	 */
	public void setFacing(Facing facing)
	{
		this.facing = facing;
	}
	
	/**
	 * Make the element turn right
	 * @see Facing
	 */
    public void rightTurn()
    {
        switch(facing){

            case NORTH :
                facing = Facing.EAST;
                break;
            case EAST :
                facing = Facing.SOUTH;
                break;
            case SOUTH :
                facing = Facing.WEST;
                break;
            case WEST :
                facing = Facing.NORTH;
                break;
            default :
                System.out.println("Unexpected error occured. Facing not changed.");
                break;
        }
    }

    /**
     * Make the element turn right
     * @see Facing
     */
    public void leftTurn()
    {
        switch(facing){

            case NORTH :
                facing = Facing.WEST;
                break;
            case WEST :
                facing = Facing.SOUTH;
                break;
            case SOUTH :
                facing = Facing.EAST;
                break;
            case EAST : 
                facing = Facing.NORTH;
                break;
            default :
                System.out.println("Unexpected error occured. Facing not changed.");
                break;
        }
    }
}
