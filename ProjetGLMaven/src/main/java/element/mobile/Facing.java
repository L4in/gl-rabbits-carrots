package element.mobile;

/**
 * Enum class for wordy facing management
 * This enumeration consist of four positions : NORTH, EAST, SOUTH, WEST
 * @author Lain
 *
 */

public enum Facing {
    NORTH, EAST, SOUTH, WEST
}
