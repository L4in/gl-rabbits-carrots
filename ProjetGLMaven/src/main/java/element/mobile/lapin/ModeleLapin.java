package element.mobile.lapin;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import service.GeneralService;

/**
 * Class for rabbit model handling
 * @author Yann Ramusat
 *
 */
public class ModeleLapin extends AbstractTableModel {

	private final String[] entetes;
	private List<Lapin> lapins;

	final String fileName = "src/main/resources/Lapin.txt";
	
	private GeneralService service = GeneralService.getInstance();

	public ModeleLapin() {
		super();

		lapins = service.getAllLapins(fileName);
		entetes = new String[] { "Nom", "Nombre de carottes mang�es" };
	}


	
	@Override
	public String getColumnName(int columnIndex) {
		return entetes[columnIndex];
	}

	@Override
	public int getRowCount() {
		return lapins.size();
	}

	@Override
	public int getColumnCount() {
		return entetes.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {	
		final Lapin lapin = lapins.get(rowIndex);

		// Ordre : "Nom", "Nombre de carottes mang�es"
		switch (columnIndex) {
		case 0:
			return lapin.getName();
		case 1:
			return lapin.getCarottesMangees();
		default:
			throw new IllegalArgumentException("Le numero de colonne indique n'est pas valide.");
		}
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
		case 0:
			return String.class;
		case 1:
			return Integer.class;
		default:
			return Object.class;
		}
	}
/*
	public void ajouterChien(final Chien chien) {
		LOGGER.debug("ajouterChien");

		chiens.add(chien);

		final int position = chiens.size() - 1;
		fireTableRowsInserted(position, position);
	}

	public void supprimerChien(final int rowIndex) {
		LOGGER.debug("supprimerChien");

		chiens.remove(rowIndex);
		fireTableRowsDeleted(rowIndex, rowIndex);
	}
*/
	/**
	 * Get the list of rabbits in the game
	 * @return	The list of rabbits
	 */
	public List<Lapin> getLapins() {
		return lapins;
	}
}
