package element;
/**
 * Abstract class of elements in a grid 
 * @author Yann Ramusat
 *
 */
public abstract class Element {
	protected int largeur;
	protected int hauteur;

	/**
	 * Get the width of the element
	 * @return	The width of the element
	 */
	public int getLargeur() {
		return largeur;
	}
	
	/**
	 * Set the width of the element
	 * @param largeur	The width to be given		
	 */
	public void setLargeur(int largeur) {
		this.largeur = largeur;
	}
	
	/**
	 * Get the height of the element
	 * @return	The width of the element
	 */
	public int getHauteur() {
		return hauteur;
	}
	
	/**
	 * Set the height of the element
	 * @param hauteur	The height of the element
	 */
	public void setHauteur(int hauteur) {
		this.hauteur = hauteur;
	}
}
