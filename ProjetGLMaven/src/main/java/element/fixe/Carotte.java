package element.fixe;

import element.Element;

/**
 * Carrot class. Using Carrot Engine 2.0
 * @author Yann Ramusat
 *
 */
public class Carotte extends Element {
	private int number;

	/**
	 * Get the number of carrots remaining
	 * @return	The number of carrots remaining
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * Set the number of carrots remaining
	 * @param number	The number of carrots we want to set
	 */
	public void setNumber(int number) {
		this.number = number;
	}

}
