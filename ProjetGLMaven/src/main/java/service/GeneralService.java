package service;

import java.io.File;
import java.util.List;

import dao.txt.TxtJardinDao;
import dao.txt.TxtLapinDao;
import element.fixe.Carotte;
import element.fixe.Rocher;
import element.mobile.lapin.Lapin;

/**
 * Singleton implementation handling list of objects
 * @author Yann Ramusat
 *
 */

public class GeneralService {
	private TxtLapinDao lapinDao;
	private TxtJardinDao jardinDao;

	/**
	 * Instanciation of the class
	 */
	private static GeneralService instance;

	/**
	 * Private constructor
	 */
	private GeneralService() {
		lapinDao = new TxtLapinDao();
		jardinDao = new TxtJardinDao();
	}

	/**
	 * Classical synchronized singleton 
	 * 
	 * @return 
	 */
	public static synchronized GeneralService getInstance() {
		if (instance == null) {
			instance = new GeneralService();
		}
		return instance;
	}

	/**
	 * Extracting list of rabbits from file
	 * @param fileName	The file containing the data
	 * @return	The list of rabbits
	 */
	public List<Lapin> getAllLapins(final String fileName) {
		final File file = new File(fileName);
		lapinDao.init(file);
		return lapinDao.getAllLapins();
	}
	
	/**
	 * Return width of garden
	 * @param fileName	The file where the data is stored
	 * @return	The width of the garden
	 */
	public int getLargeur(final String fileName) {
		final File file = new File(fileName);
		jardinDao.init(file);
		return jardinDao.getLargeur();
	}
	
	/**
	 * Return height of garden
	 * @param fileName	The file where the data is stored
	 * @return	The width of the garden
	 */
	public int getHauteur(final String fileName) {
		final File file = new File(fileName);
		jardinDao.init(file);
		return jardinDao.getHauteur();
	}
	
	/**
	 * Return the list of all the carrots
	 * @param fileName	The file where the data is stored
	 * @return	The list of carrots
	 */
	public List<Carotte> getAllCarottes(final String fileName) {
		final File file = new File(fileName);
		jardinDao.init(file);
		return jardinDao.getAllCarottes();
	}
	
	/**
	 * Return the list of all the rocks
	 * @param fileName	The file where the data is stored
	 * @return	The list of rocks
	 */
    public List<Rocher> getAllRochers(final String fileName) {
		final File file = new File(fileName);
		jardinDao.init(file);
		return jardinDao.getAllRochers();
	}
}
