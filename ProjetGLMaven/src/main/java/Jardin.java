
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import element.fixe.Carotte;
import element.fixe.Rocher;
import element.mobile.Facing;
import service.GeneralService;
/**
 * Class for garden emulation, filled with rabbits, rocks, carrots and foxes
 * @author Yann Ramusat
 *
 */
public class Jardin extends JPanel {
	private GeneralService service = GeneralService.getInstance();
	final String fileName = "src/main/resources/Jardin.txt";
	// Image lapin
	public static final String LAPINN = "img/lapinN.png"; 
	private BufferedImage lapinN;
	public static final String LAPINO = "img/lapinO.png"; 
	private BufferedImage lapinO;
	public static final String LAPINS = "img/lapinS.png"; 
	private BufferedImage lapinS;
	public static final String LAPINE = "img/lapinE.png"; 
	private BufferedImage lapinE;
	// Image carotte
	public static final String CAROTTE = "img/carotte.png"; 
	private BufferedImage carotte;
	// Image rocher
	public static final String ROCK = "img/rock.png";
	private BufferedImage rock;
	
	private int caseSize = 40;
	
	public Case[][] cases;
	
	public Jardin()
	{
		// Chargement des images
		try
		{
			lapinN = ImageIO.read(new File(LAPINN));
			lapinO = ImageIO.read(new File(LAPINO));
			lapinS = ImageIO.read(new File(LAPINS));
			lapinE = ImageIO.read(new File(LAPINE));
			carotte = ImageIO.read(new File(CAROTTE));
			rock = ImageIO.read(new File(ROCK));
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		
		cases = new Case[service.getHauteur(fileName)][service.getLargeur(fileName)];
		
		for(int i = 1; i <= cases[0].length; i++)
		{
			for(int j = 1; j <= cases.length; j++)
			{
				cases[j-1][i-1] = new Case();
			}
		}
		
		List<Carotte> carottes = new LinkedList<Carotte>();
		carottes = service.getAllCarottes(fileName);
		for(int i = 0; i < carottes.size(); i++)
		{
			if(carottes.get(i).getHauteur() >= cases.length || carottes.get(i).getLargeur() >= cases[0].length || carottes.get(i).getHauteur() < 1 || carottes.get(i).getLargeur() < 1) 
				carottes.remove(carottes.get(i));
			else
				cases[carottes.get(i).getHauteur()-1][carottes.get(i).getLargeur()-1].setNumberOfCarottes(carottes.get(i).getNumber());
		}
		
		List<Rocher> rochers = new LinkedList<Rocher>();
		rochers = service.getAllRochers(fileName);
		for(int i = 0; i < rochers.size(); i++)
		{
			if(rochers.get(i).getHauteur() >= cases.length || rochers.get(i).getLargeur() >= cases[0].length || rochers.get(i).getHauteur() < 1 || rochers.get(i).getLargeur() < 1) 
				rochers.remove(rochers.get(i));
			else
			cases[rochers.get(i).getHauteur()-1][rochers.get(i).getLargeur()-1].setRocher(true);
		}
	}
	
	/**
	 * Return the garden's height
	 * @return	The garden's height
	 */
	public int getHauteur()
	{
		return cases.length;
	}
	
	/**
	 * Get the garden's width
	 * @return	The garden's width
	 */
	public int getLargeur()
	{
		return cases[0].length;
	}
	
	/**
	 * Get the size of one box's size
	 * @return	The size of a box's size
	 */
	public int getCaseSize()
	{
		return this.caseSize;
	}
	
	@Override
	public int getWidth() // Width of the component
	{ 
		return (cases[0].length+1)*this.caseSize+1;
	}
	
	@Override
	public int getHeight() // Height of the component
	{ 
		return (cases.length+1)*this.caseSize+1;
	}
	
	@Override
	public Dimension getPreferredSize() // Preferred size of the component
	{ 
		return	new Dimension(getWidth(), getHeight());
	}
	
	@Override
	public void paint(Graphics g)
	{ 
		super.paint(g);
		
		float[] hsbValues = new float[3];
		hsbValues = Color.RGBtoHSB(146,208,80,hsbValues);
		g.setColor(Color.getHSBColor(hsbValues[0],hsbValues[1],hsbValues[2]));
		
		g.fillRect(this.caseSize,this.caseSize, getWidth(), getHeight());
		
		for(int i = 1; i <= cases[0].length; i++)
		{
			for(int j = 1; j <= cases.length; j++)
			{
				if(cases[j-1][i-1].getNumberOfCarottes() > 0)
				{
					g.setColor(Color.ORANGE);
					g.fillRect(i*this.caseSize+1,j*caseSize+1, this.caseSize-1, this.caseSize-1);
					g.drawImage(carotte, i*this.caseSize+4,j*caseSize+4, null);
				}
				
				if(cases[j-1][i-1].getRocher())
				{
					//g.setColor(Color.BLACK);
					//g.fillRect(i*this.caseSize+1,j*caseSize+1, this.caseSize-1, this.caseSize-1);
					g.drawImage(rock, i*this.caseSize+2,j*caseSize+7, null);
				}
				
				if(cases[j-1][i-1].getOccuped())
				{
					if(cases[j-1][i-1].getFacingOn() == Facing.NORTH)
					{
						g.drawImage(lapinN, i*this.caseSize+11,j*caseSize+4, null);
					}
					else if (cases[j-1][i-1].getFacingOn() == Facing.WEST)
					{
						g.drawImage(lapinO, i*this.caseSize+11,j*caseSize+4, null);
					}
					else if (cases[j-1][i-1].getFacingOn() == Facing.SOUTH)
					{
						g.drawImage(lapinS, i*this.caseSize+11,j*caseSize+4, null);
					}
					else if (cases[j-1][i-1].getFacingOn() == Facing.EAST)
					{
						g.drawImage(lapinE, i*this.caseSize+11,j*caseSize+4, null);
					}
				}
				
				g.setColor(Color.BLACK);
				g.drawRect(i*this.caseSize,j*caseSize, this.caseSize, this.caseSize);
			}
		}
	}
}
