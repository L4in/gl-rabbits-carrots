import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import element.mobile.Facing;
import element.mobile.lapin.Lapin;
import element.mobile.lapin.ModeleLapin;
import element.mobile.renard.Renard;

/**
 * Class integrating the main of the game and the functional loop for the game
 * @author Yann Ramusat
 *
 */

public class Game extends JFrame implements ActionListener {
	public Jardin jardin;
	public ModeleLapin lapins = new ModeleLapin();
	public List<Renard> renard = new ArrayList<Renard>();
	JButton genetic;
	JButton stats;
	public JFrame frame;
	JPanel topPanel;
	JPanel lowPanel;
	
	public String gameState = "basic"; // basic wait genetic
	
	private final JTable tableau;
	
	/**
	 * Constructor of Game, inistialize the GUI
	 */
	public Game()
	{
		// Init du JPanel jardin
		jardin = new Jardin();
		
		// Affichage des lapins pour la premi�re occurence de run(), g�re les v�rifications des donn�es des lapins
		for(int i = 0; i < lapins.getLapins().size(); i++)
		{
			if(lapins.getLapins().get(i).getHauteur() >= jardin.cases.length || lapins.getLapins().get(i).getLargeur() >= jardin.cases[0].length || lapins.getLapins().get(i).getHauteur() < 1 || lapins.getLapins().get(i).getLargeur() < 1) 
				lapins.getLapins().remove(lapins.getLapins().get(i));
			else
			{
				if (!jardin.cases[lapins.getLapins().get(i).getHauteur()-1][lapins.getLapins().get(i).getLargeur()-1].getOccuped())
				{
					jardin.cases[lapins.getLapins().get(i).getHauteur()-1][lapins.getLapins().get(i).getLargeur()-1].setOccuped(true);
					jardin.cases[lapins.getLapins().get(i).getHauteur()-1][lapins.getLapins().get(i).getLargeur()-1].setFacingOn(lapins.getLapins().get(i).getFacing());
				}
				else
					lapins.getLapins().remove(lapins.getLapins().get(i));
			}
		}
		
		// RENDU GRAPHIQUE
		JFrame frame = new JFrame("Genetic Rabbits");
		frame.setPreferredSize(new Dimension(640,480));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		   
	    // CENTRER AFFICHAGE
		frame.add(this.jardin, BorderLayout.CENTER);

		topPanel = new JPanel();
	    topPanel.setLayout(new GridLayout(2,1));
	    
		    tableau = new JTable(lapins);
		    topPanel.add(tableau.getTableHeader(), BorderLayout.NORTH);
		    topPanel.add(tableau, BorderLayout.NORTH);
		        
		frame.add(topPanel, BorderLayout.NORTH);
		
		lowPanel = new JPanel();
	    lowPanel.setLayout(new GridLayout(2,1));
	    
			stats = new JButton("Analyser les statistiques");
			stats.setActionCommand("stats");
		    stats.addActionListener(this);
		    stats.setVisible(false);
			lowPanel.add(stats, BorderLayout.NORTH);
			
			genetic = new JButton("Commencer le mode Genetic");
			genetic.setActionCommand("genetic");
		    genetic.addActionListener(this);
			genetic.setEnabled(false);
			lowPanel.add(genetic, BorderLayout.SOUTH);

			frame.add(lowPanel, BorderLayout.SOUTH);
		    
        frame.pack();
		frame.setVisible(true);
	}
	
	/**
	 * Loop of the game refreshing GUI and making the simulation go forward
	 */
	public void play()
	{
		try {
			Thread.sleep(500);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		while(true)
		{
			if(gameState == "basic")
			{
				Boolean end = true;
				for(Lapin lapin : lapins.getLapins())
				{
					if(!lapin.isPaused()) end = false;
					if(!lapin.isEat() && jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].getNumberOfCarottes() > 0)
					{
						lapin.mangeCarotte();
						jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].carotteMangee();
					}
					else if (lapin.lookMouvement() == 'G')
					{
						lapin.getMouvement();
						lapin.leftTurn();
						jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setFacingOn(lapin.getFacing());
					}
					else if (lapin.lookMouvement() == 'D')
					{
						lapin.getMouvement();
						lapin.rightTurn();
						jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setFacingOn(lapin.getFacing());
					}
					else if (lapin.lookMouvement() == 'A')
					{
						// GERER L'AVANCEMENT
						if(lapin.getFacing() == Facing.NORTH)
						{
							if(lapin.getHauteur() > 1)
							{
								if(!jardin.cases[lapin.getHauteur()-2][lapin.getLargeur()-1].getRocher())
								{
									if(!jardin.cases[lapin.getHauteur()-2][lapin.getLargeur()-1].getOccuped())
									{
										// D�placement du lapin
										jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setOccuped(false);
										lapin.setHauteur(lapin.getHauteur()-1);
										jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setOccuped(true);
										
										// G�rer les nouveaux contenus des �tats du lapin et des cases
										lapin.getMouvement();
										lapin.setEat(false);
										jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setFacingOn(lapin.getFacing());
									}
									else
										lapin.setPaused(true);
								}
								else
									lapin.getMouvement();
							}
							else
								lapin.getMouvement();
						}
						else if (lapin.getFacing() == Facing.WEST)
						{
							if(lapin.getLargeur() > 1)
							{
								if(!jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-2].getRocher())
								{
									if(!jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-2].getOccuped())
									{
										// D�placement du lapin
										jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setOccuped(false);
										lapin.setLargeur(lapin.getLargeur()-1);
										jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setOccuped(true);
										
										// G�rer les nouveaux contenus des �tats du lapin et des cases
										lapin.getMouvement();
										lapin.setEat(false);
										jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setFacingOn(lapin.getFacing());
									}
									else
										lapin.setPaused(true);
								}
								else
									lapin.getMouvement();
							}
							else
								lapin.getMouvement();
						}
						else if (lapin.getFacing() == Facing.SOUTH)
						{
							if(lapin.getHauteur() < jardin.getHauteur())
							{
								if(!jardin.cases[lapin.getHauteur()][lapin.getLargeur()-1].getRocher())
								{
									if(!jardin.cases[lapin.getHauteur()][lapin.getLargeur()-1].getOccuped())
									{
										// D�placement du lapin
										jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setOccuped(false);
										lapin.setHauteur(lapin.getHauteur()+1);
										jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setOccuped(true);
										
										// G�rer les nouveaux contenus des �tats du lapin et des cases
										lapin.getMouvement();
										lapin.setEat(false);
										jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setFacingOn(lapin.getFacing());
									}
									else
										lapin.setPaused(true);
								}
								else
									lapin.getMouvement();
							}
							else
								lapin.getMouvement();	
						}
						else if (lapin.getFacing() == Facing.EAST)
						{
							if(lapin.getLargeur() < jardin.getLargeur())
							{
								if(!jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()].getRocher())
								{
									if(!jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()].getOccuped())
									{
										// D�placement du lapin
										jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setOccuped(false);
										lapin.setLargeur(lapin.getLargeur()+1);
										jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setOccuped(true);
										
										// G�rer les nouveaux contenus des �tats du lapin et des cases
										lapin.getMouvement();
										lapin.setEat(false);
										jardin.cases[lapin.getHauteur()-1][lapin.getLargeur()-1].setFacingOn(lapin.getFacing());
									}
									else
										lapin.setPaused(true);
								}
								else
									lapin.getMouvement();
							}
							else
								lapin.getMouvement();
						}
					}
					if(end) 
					{
						gameState = "wait";
						genetic.setEnabled(true);
					}
				}
				
				try
				{
					Thread.sleep(1000);
				} catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
			
			if(gameState == "wait")
			{
				
			}
			
			if(gameState == "genetic")
			{
				// AVANCER LES LAPINS
				// BOUCLER SUR LES RENARDS
					// AVANCER LES RENARDS
					// TEST CASES A COTE
					// LANCER RENCONTRE
					// METTRE STATS A JOUR
			}
			
			// Mise a jour graphique
			jardin.updateUI();
			lapins.fireTableDataChanged();
		}
	}
	
	/**
	 * MAIN of the program doing nothing except launching play()
	 * @param args	The array of strings containing arguments given
	 */
	public static void main(String[] args)
	{
		new Game().play();
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		switch(e.getActionCommand()) 
		{ 
			case "genetic": 	
				initialiseGeneticStep();
				gameState = "genetic";
				break;
		}
	}
	
	/**
	 * Launch the game in genetic mode
	 */
	public void initialiseGeneticStep()
	{
		topPanel.setVisible(false);
		stats.setVisible(true);
		
		// initialiser la liste des renards
	}
}
